Source: sfcgal
Maintainer: Debian GIS Project <pkg-grass-devel@lists.alioth.debian.org>
Uploaders: Sven Geggus <sven-debian@geggus.net>,
           Bas Couwenberg <sebastic@debian.org>
Section: science
Priority: optional
Build-Depends: chrpath,
               cmake,
               debhelper-compat (= 13),
               dh-sequence-pkgkde-symbolshelper,
               libcgal-dev (>= 5.3),
               libboost-all-dev,
               libmpfr-dev,
               libgmp-dev,
               pkg-kde-tools
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/debian-gis-team/sfcgal
Vcs-Git: https://salsa.debian.org/debian-gis-team/sfcgal.git
Homepage: http://www.sfcgal.org/
Rules-Requires-Root: no

Package: libsfcgal2
Architecture: any
Section: libs
Depends: ${shlibs:Depends},
         ${misc:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: Library for ISO 19107:2013 and OGC SFA 1.2 for 3D operations
 SFCGAL is a C++ wrapper library around CGAL with the aim of
 supporting ISO 19107:2013 and OGC Simple Features Access 1.2 for 3D
 operations.
 .
 SFCGAL provides standard compliant geometry types and operations,
 that can be accessed from its C or C++ APIs. PostGIS uses the C API,
 to expose some SFCGAL's functions in spatial databases (cf. PostGIS
 manual).
 .
 This package provides the shared library.

Package: libsfcgal-dev
Architecture: any
Section: libdevel
Depends: libsfcgal2 (= ${binary:Version}),
         libcgal-dev (>= 4.10.1),
         libgmp-dev,
         ${misc:Depends}
Description: Library for ISO 19107:2013 and OGC SFA 1.2 for 3D operations (development)
 SFCGAL is a C++ wrapper library around CGAL with the aim of
 supporting ISO 19107:2013 and OGC Simple Features Access 1.2 for 3D
 operations.
 .
 SFCGAL provides standard compliant geometry types and operations,
 that can be accessed from its C or C++ APIs. PostGIS uses the C API,
 to expose some SFCGAL's functions in spatial databases (cf. PostGIS
 manual).
 .
 This package provides the development files.
