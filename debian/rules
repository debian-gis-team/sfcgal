#!/usr/bin/make -f

include /usr/share/dpkg/architecture.mk
include /usr/share/dpkg/pkg-info.mk

DH_VERBOSE = 1

UPSTREAM_VERSION=$(shell echo $(DEB_VERSION_UPSTREAM) | sed -e 's/\+.*//')

export DEB_BUILD_MAINT_OPTIONS=hardening=+all

# reduce debug info to fix FTBFS on archs with few address space
ifneq (,$(filter $(DEB_HOST_ARCH),arm64 armhf mips mipsel ppc64el))
    CMAKECXXFLAGS="-DCMAKE_CXX_FLAGS_RELWITHDEBINFO=' -O2 -g1 -DNDEBUG'"
endif

# main packaging script based on dh7 syntax
%:
	dh $@

override_dh_auto_configure:
	dh_auto_configure -- \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		$(CMAKECXXFLAGS) \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib/$(DEB_HOST_MULTIARCH) \
		-DCMAKE_VERBOSE_MAKEFILE=ON \
		-DSFCGAL_BUILD_TESTS=ON

override_dh_auto_test:
ifneq (,$(filter $(DEB_BUILD_ARCH),s390x hppa m68k powerpc ppc64 sparc64))
	# Skip tests
else ifneq (,$(filter $(DEB_BUILD_ARCH),arm64 mips64el ppc64el hurd-i386 loong64))
	dh_auto_test || echo "Ignoring test failures"
else
	dh_auto_test
endif

execute_before_dh_install:
	# removing embedded rpath in binaries
	-find $(CURDIR)/debian/tmp/usr/bin -type f -not -name sfcgal-config -exec chrpath --delete {} \;

	# removing embedded rpath in libraries
	-find $(CURDIR)/debian/tmp/usr/lib -name "*.so*" -type f -exec chrpath --delete {} \;

	# Remove test executables
	$(RM) $(CURDIR)/debian/tmp/usr/bin/*test*

override_dh_installchangelogs:
	dh_installchangelogs NEWS
	
override_dh_makeshlibs:
	dh_makeshlibs -- -c0 -v$(UPSTREAM_VERSION)
